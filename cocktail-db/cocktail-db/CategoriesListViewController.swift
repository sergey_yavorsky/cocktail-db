//
//  CategoriesListViewController.swift
//  cocktail-db
//
//  Created by Sergey Yavorsky on 12/6/19.
//  Copyright © 2019 ltst. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage

protocol CategoriesListProtocol {
    func updateControllerWithFilterState(_ filter: [Filter])
}

struct Filter {
    
    var name: String?
    var isActive = true
    
}

class CategoriesListViewController: BaseViewController {
    
    // MARK: - Properties & IBOutlets
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var categories: [CategoryItemModel]?
    private var rightBarButton = UIButton()
    private var catigoriesCount = 0
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Drinks"
        catigoriesList()
    }
    
    // MARK: - Private methods
    
    private func drawRightBarButton() {
        rightBarButton = UIButton(type: UIButton.ButtonType.custom)
        rightBarButton.setImage(R.image.filterNoActive(), for: .normal)
        rightBarButton.addTarget(self, action:#selector(self.callMethod), for: .touchUpInside)
        rightBarButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        let barButton = UIBarButtonItem(customView: rightBarButton)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc private func callMethod() {
        let vc = R.storyboard.main.filterViewController()!
        vc.categoriesDelegate = self
        var filter: [Filter] = []
        for index in 0..<catigoriesCount {
            let categoty = categories![index]
            if let name = categoty.name {
                filter.append(Filter(name: name, isActive: categoty.isActive))
            }
        }
        vc.externalFilter = filter
        navigationController?.pushViewController(vc, animated: true)
    }
    private func showSpinner() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
    }
    private func loadingIsFinished() {
        MBProgressHUD.hide(for: self.view, animated: true)
        self.drawRightBarButton()
        self.tableView.isHidden = false
    }
    private func loadIconForCell(cell: CategoryItemCell, url: URL) {
        SDWebImageManager.shared.loadImage(with: url, options: .continueInBackground, progress: nil) { [weak self] (image: UIImage?, _, error: Error?, _, _, _)  in
            guard self != nil else { return }
            if let image = image {
                cell.productIconImgView.image = image
            }
        }
    }
}

// MARK: - Connections extension

extension CategoriesListViewController {
    
    private func catigoriesList() {
        showSpinner()
        DrinksProvider.request(.listOfCatrgories, completion: { [weak self] result in
            guard let `self` = self else { return }
            switch result {
            case .success(let response):
                if response.statusCode == 200 {
                    do {
                        let categoriesModel = try JSONDecoder().decode(CategoriesModel.self, from: response.data)
                        if let drinks = categoriesModel.drinks, drinks.count > 0 {
                            self.categories = drinks
                            self.catigoriesCount = 1
                            self.tableView.reloadData()
                        } else {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                    } catch {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                }
            case .failure(_):
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        })
    }
    private func loadCatertyItemsBy(_ name: String,_ index: Int) {
        DrinksProvider.request(.filterByCategory(name: name), completion: { [weak self] result in
            guard let `self` = self else { return }
            switch result {
            case .success(let response):
                if response.statusCode == 200 {
                    do {
                        let drinkModel = try JSONDecoder().decode(DrinkModel.self, from: response.data)
                        if let drinks = drinkModel.drinks, drinks.count > 0 {
                            self.categories![index].drinks = drinks
                            self.tableView.reloadData()
                        } else {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                    } catch {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                }
            case .failure(_):
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        })
    }
    
}

// MARK: - UITableViewDataSource & UIScrollViewDelegate

extension CategoriesListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return catigoriesCount
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let categoty = self.categories, let drinks = categoty[section].drinks else { return 0 }
        return drinks.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryHeaderCell") as! CategoryHeaderCell
        guard let drinks = self.categories, let name = drinks[section].name  else { return cell }
        cell.titleLbl.text = name.uppercased()
        return cell
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.item
        let section = indexPath.section
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.categoryItemCell, for: indexPath)!
        guard
            let categories = self.categories,
            let drinks = categories[section].drinks,
            let image = drinks[index].image,
            let name = drinks[index].name
            else { return cell }
        cell.titleLbl.text = name
        loadIconForCell(cell: cell, url: URL(string: image)!)
        cell.sepratorView.isHidden = index == drinks.count - 1 ? true : false
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let categories = self.categories, categories.count > 0 else { return }
        if categories[section].drinks == nil {
            self.loadCatertyItemsBy(categories[section].name ?? "", section)
            if section == 0 {
                loadingIsFinished()
            }
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let index = indexPath.row
        guard
            let categories = self.categories,
            categories.count > 0,
            let drinks = categories[section].drinks,
            drinks.count > 0
            else { return }
        if index == drinks.count - 1 &&
            catigoriesCount == section + 1 &&
            categories.count > catigoriesCount {
            catigoriesCount += 1
            tableView.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let drinks = self.categories, drinks.count > 0 else { return 0 }
        return drinks[indexPath.section].isActive ? 88 : 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let drinks = self.categories, drinks.count > 0 else { return 0 }
        return drinks[section].isActive ? 30 : 0
    }
    
}

// MARK: - Protocol extension

extension CategoriesListViewController: CategoriesListProtocol {
    
    func updateControllerWithFilterState(_ filter: [Filter]) {
        var isFilterActive = false
        for index in 0..<catigoriesCount {
            let internalItem = categories![index]
            let externalItem = filter[index]
            if internalItem.name == externalItem.name {
                categories![index].isActive = externalItem.isActive
            }
            if externalItem.isActive == false {
                isFilterActive = true
            }
        }
        rightBarButton.setImage(isFilterActive ? R.image.filterActive() : R.image.filterNoActive(), for: .normal)
        tableView.reloadData()
    }
    
}

// MARK: - Class cell's

class CategoryHeaderCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    
}

class CategoryItemCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var productIconImgView: UIImageView!
    @IBOutlet weak var sepratorView: UIView!
    
    // TODO: Problem with Zeplin - can't get separation parameters for last element.
    // @IBOutlet weak var finishSepratorView: UIView!
    
}



