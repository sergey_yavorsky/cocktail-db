//
//  TestAPI.swift
//  cocktail-db
//
//  Created by Sergey Yavorsky on 12/6/19.
//  Copyright © 2019 ltst. All rights reserved.
//

import Moya

let DrinksProvider = MoyaProvider<DrinksService>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])

public enum DrinksService {
    case listOfCatrgories
    case filterByCategory(name: String)
}

extension DrinksService: TargetType {
    
    public var baseURL: URL {
        switch self {
        case .listOfCatrgories:
            return URL(string: mainURL() + "list.php?c=list")!
        case .filterByCategory(_):
            return URL(string: mainURL() + "filter.php")!
        }
    }

    public var path: String {
        switch self {
        case .listOfCatrgories:
            return ""
        case .filterByCategory(_):
            return ""
        }
    }

    public var method: Moya.Method {
        return .get
    }

    public var task: Task {
        switch self {
        case .listOfCatrgories:
            return .requestPlain
        case .filterByCategory(let name):
            let parameterEncoding = URLEncoding(destination: .httpBody)
            return .requestCompositeParameters(bodyParameters: [:], bodyEncoding: parameterEncoding, urlParameters: ["c": name])
        }
    }

    public var validate: Bool {
        return false
    }

    public var sampleData: Data {
        return Data()
    }

    public var headers: [String: String]? {
        switch self {
        default:
            return Headers
        }
    }
    
}
