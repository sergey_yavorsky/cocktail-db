//
//  API.swift
//  cocktail-db
//
//  Created by Sergey Yavorsky on 12/6/19.
//  Copyright © 2019 ltst. All rights reserved.
//

import Moya

let BaseHost = "https://www.thecocktaildb.com/"
let HostAPI = "api/json/v1/1/"

public func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data
    }
}

public func mainURL() -> String {
    return BaseHost + HostAPI
}

public var Headers: [String : String]? {
    let assigned: [String: String] = [
        "accept": "application/json"
        ]
    return assigned
}
