//
//  DrinkModel.swift
//  cocktail-db
//
//  Created by Work on 07.12.2019.
//  Copyright © 2019 ltst. All rights reserved.
//

import Foundation

struct DrinkModel: Decodable {
    
    let drinks: [DrinkItemModel]?
    
}

struct DrinkItemModel: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case strDrink, strDrinkThumb, idDrink
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .strDrink)
        image = try container.decode(String.self, forKey: .strDrinkThumb)
        id = try container.decode(String.self, forKey: .idDrink)
    }
    
    let name: String?
    let image: String?
    let id: String?
    
}
