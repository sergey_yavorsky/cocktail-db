//
//  CategoryModel.swift
//  cocktail-db
//
//  Created by Work on 07.12.2019.
//  Copyright © 2019 ltst. All rights reserved.
//

import Foundation

struct CategoriesModel: Decodable {
    
    var drinks: [CategoryItemModel]?
    
}

struct CategoryItemModel: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case strCategory
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .strCategory)
        isActive = true
        drinks = nil
    }
    
    let name: String?
    
    // Custom value's
    var drinks: [DrinkItemModel]?
    var isActive: Bool
    
}
