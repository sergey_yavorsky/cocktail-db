//
//  FilterViewController.swift
//  cocktail-db
//
//  Created by Sergey Yavorsky on 12/6/19.
//  Copyright © 2019 ltst. All rights reserved.
//

import UIKit

class FilterViewController: BaseViewController {
    
    // MARK: - Properties & IBOutlets
    
    var externalFilter = [Filter]() {
        didSet {
            filter = externalFilter
        }
    }
    var categoriesDelegate: CategoriesListProtocol!
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var filter: [Filter]?
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Filters"
    }
    
    // MARK: - Private methods
    
    @IBAction private func applyFilter(_ sender: Any) {
        for (indexI , elementI) in externalFilter.enumerated() {
            guard let filterState = filter, filterState.count > 0  else { return }
            let externalItemState = elementI.isActive
            let internalItemState = filterState[indexI].isActive
            if externalItemState != internalItemState {
                categoriesDelegate.updateControllerWithFilterState(filterState)
                navigationController?.popViewController(animated: true)
                break
            }
        }
    }
}

// MARK: - UITableViewDataSource & UIScrollViewDelegate

extension FilterViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let filter = filter  else { return 0 }
        return filter.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.filterItemCell, for: indexPath)!
        cell.selectionStyle = .none
        let index = indexPath.item
        let filterItem = filter![index]
        cell.titleLbl.text = filterItem.name
        cell.sepratorView.isHidden = filter!.count == index - 1 ? true : false
        cell.checkImgView.isHidden = filterItem.isActive ? false : true
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.item
        self.filter![index].isActive = !filter![index].isActive
        tableView.reloadData()
    }
    
}

// MARK: - Class cell's

class FilterItemCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var checkImgView: UIImageView!
    
    // TODO: Problem with Zeplin - can't get separation parameters for last element.
    // @IBOutlet weak var finishSepratorView: UIView!
    
}
