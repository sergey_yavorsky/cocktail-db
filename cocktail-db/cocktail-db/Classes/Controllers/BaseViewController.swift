//
//  BaseViewController.swift
//  cocktail-db
//
//  Created by Sergey Yavorsky on 12/6/19.
//  Copyright © 2019 ltst. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    // MARK: - Lifecycle methods

	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: R.font.sfProTextSemibold(size: 17)!]
	}
	
}
