//
//  View.swift
//  cocktail-db
//
//  Created by Sergey Yavorsky on 12/6/19.
//  Copyright © 2019 ltst. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableView: UIView {
}

@IBDesignable
class DesignableButton: UIButton {
}

@IBDesignable
class DesignableLabel: UILabel {
}

extension UIView {

	@IBInspectable
	var cornerRadius: CGFloat {
		get {
			return layer.cornerRadius
		}
		set {
			layer.cornerRadius = newValue
		}
	}

	@IBInspectable
	var borderWidth: CGFloat {
		get {
			return layer.borderWidth
		}
		set {
			layer.borderWidth = newValue
		}
	}

	@IBInspectable
	var borderColor: UIColor? {
		get {
			if let color = layer.borderColor {
				return UIColor(cgColor: color)
			}
			return nil
		}
		set {
			if let color = newValue {
				layer.borderColor = color.cgColor
			} else {
				layer.borderColor = nil
			}
		}
	}
    
}

@IBDesignable class ShadowView: UIView {

	@IBInspectable var shadowColor: UIColor? {
		get {
			if let color = layer.shadowColor {
				return UIColor(cgColor: color)
			}
			return nil
		}
		set {
			if let color = newValue {
				layer.shadowColor = color.cgColor
			} else {
				layer.shadowColor = nil
			}
		}
	}

	@IBInspectable var shadowOpacity: Float {
		get {
			return layer.shadowOpacity
		}
		set {
			layer.shadowOpacity = newValue
		}
	}

	@IBInspectable var shadowOffset: CGPoint {
		get {
			return CGPoint(x: layer.shadowOffset.width, y:layer.shadowOffset.height)
		}
		set {
			layer.shadowOffset = CGSize(width: newValue.x, height: newValue.y)
		}

	 }

	@IBInspectable var shadowBlur: CGFloat {
		get {
			return layer.shadowRadius
		}
		set {
			layer.shadowRadius = newValue / 2.0
		}
	}

	@IBInspectable var shadowSpread: CGFloat = 0 {
		didSet {
			if shadowSpread == 0 {
				layer.shadowPath = nil
			} else {
				let dx = -shadowSpread
				let rect = bounds.insetBy(dx: dx, dy: dx)
				layer.shadowPath = UIBezierPath(rect: rect).cgPath
			}
		}
	}
    
}
