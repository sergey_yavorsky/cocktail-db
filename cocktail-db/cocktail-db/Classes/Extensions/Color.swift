//
//  Color.swift
//  cocktail-db
//
//  Created by Sergey Yavorsky on 12/6/19.
//  Copyright © 2019 ltst. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func RGBA(_ r: CGFloat, _ g: CGFloat, _ b: CGFloat, _ a: CGFloat) -> UIColor {
        return UIColor(red: r / CGFloat(255.0), green: g / CGFloat(255.0), blue: b / CGFloat(255.0), alpha: a)
    }
    static func RGB(_ r: CGFloat, _ g: CGFloat, _ b: CGFloat) -> UIColor {
        return RGBA(r, g, b, CGFloat(1.0))
    }
    
}
